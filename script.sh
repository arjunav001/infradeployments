#!/bin/bash
for i in $(seq 1 100)
do
    sleep 0.007
    echo $i
done | whiptail --title 'checking compatibility' --gauge 'Running...' 6 40 0

cat /etc/os-release  | grep 'NAME="Ubuntu"' > /dev/null
if [ $? -eq 0 ]; then

if (whiptail --title "Install Automation Tools" --yesno "Choose between Yes and No." 10 60) then
    echo "You chose Yes. Exit status was $?."
    sudo apt-get update -y
	sudo apt-get install lolcat toilet -y 
function jenkins() {
		sudo apt install openjdk-8-jdk -y
		sudo wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
		sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
		sudo apt-get update
		sudo apt-get install jenkins -y
		sudo systemctl start  jenkins
		sudo systemctl enable jenkins
		sudo apt-get install git -y
		sudo apt install maven -y
		sudo hostnamectl set-hostname Dev-Master
}


function rabbitmq() {
		sudo apt-key adv --keyserver "hkps.pool.sks-keyservers.net" --recv-keys "0x6B73A36E6026DFCA"
		sudo wget -O - "https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc" | sudo apt-key add -
		sudo apt-get install erlang -y
		sudo apt-get install curl gnupg -y
		sudo curl -fsSL https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc | sudo apt-key add -
		sudo apt-get install apt-transport-https -y
		sudo rm -rf /etc/apt/sources.list.d/bintray.rabbitmq.list
		sudo touch /etc/apt/sources.list.d/bintray.rabbitmq.list
		echo " deb https://dl.bintray.com/rabbitmq-erlang/debian bionic erlang ">> /etc/apt/sources.list.d/bintray.rabbitmq.list
		echo " deb https://dl.bintray.com/rabbitmq/debian bionic main " >> /etc/apt/sources.list.d/bintray.rabbitmq.list
		sudo apt-get update -y
		sudo apt-get install rabbitmq-server -y --fix-missing
		sudo  systemctl start rabbitmq-server.service
		sudo systemctl enable rabbitmq-server
}


function nginx_git() {
		sudo apt-get install nginx -y
		sudo apt-get install git -y
		sudo systemctl start git
		sudo systemctl enable  nginx
}

function docker() {
		sudo  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
		sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
		sudo apt-get update
		sudo apt-cache policy docker-ce -y
		sudo apt install docker-ce -y
		sudo systemctl start docker
		sudo usermod -aG docker ${USER}
		sudo usermod -aG docker ubuntu
		sudo systemctl enable docker
		sudo chmod a+rw /var/run/docker.sock
}


function orchestration() {
		sudo apt install unzip -y
		sudo wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip
		sudo unzip ./terraform_0.11.13_linux_amd64.zip -d /usr/local/bin/
		sudo apt install python-pip -y
		sudo pip install boto
		sudo pip install boto3
		sudo apt-get install awscli -y
		sudo apt-add-repository ppa:ansible/ansible -y
		sudo apt-get update
		sudo apt-get install ansible -y
}


function databases() {
		sudo apt-get  install mysql-server -y
		sudo apt-get install -y mongodb
		sudo systemctl enable mysql
		sudo systemctl enable mongodb
}


function nodejs() { 
		sudo curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
		sudo curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash    		    
		sudo apt-get install nodejs npm 
		source ~/.profile
		source .bashrc 
}

function jenkins_slave() { 
sudo apt install java-1.8.0-openjdk
adduser jenkins --home /var/lib/jenkins --shell /bin/bash --disabled-password 
runuser -l jenkins -c  'ssh-keygen -t rsa -C "Jenkins agent key"' 
runuser -l jenkins -c "cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys"
sudo chmod 600 var/lib/jenkins/.ssh/authorized_keys
echo "copy the follwing key to jenkins console"
sudo cat /var/lib/jenkins/.ssh/id_rsa
echo "toilet -F border MasterSlave  | lolcat" >> /etc/bash.bashrc
sudo apt install maven -y

}

function monitering() {
apt install lolcat toilet -y
DD_AGENT_MAJOR_VERSION=7 DD_API_KEY=e3fdfdd371ee80235894bbde86f7c548 bash -c "$(curl -L https://raw.githubusercontent.com/DataDog/datadog-agent/master/cmd/agent/install_script.sh)"
curl --tlsv1.2 --silent --show-error --header 'x-connect-key: 34910079b0c9176f7ec97e7c0013bd4ac155a013' https://kickstart.jumpcloud.com/Kickstart | sudo bash

}

function post_install() { 
	if (whiptail --title "Post Installation Setup " --yesno "Choose between Yes and No." 10 60) then
		echo "Configure Mysql database"
		sleep 2
		sudo mysql_secure_installation  
		sudo rabbitmq-plugins enable rabbitmq_management rabbitmq_management_agent
		PASSWORD=$(whiptail --title "Rabbitmq Passwordi for user admin" --passwordbox "Enter your password and choose Ok to continue." 10 60 3>&1 1>&2 2>&3)
		
		exitstatus=$?
		if [ $exitstatus = 0 ]; then
		    sudo rabbitmqctl add_user admin  $PASSWORD
		    sudo rabbitmqctl add_user admin qcguhj8ff6
		    sudo rabbitmqctl set_user_tags admin administrator                         
		    sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
		else
	    	    echo "Password not configured"
		fi
	fi
}

OPTION=$(whiptail --title "Test Menu Dialog" --menu "Choose your option" 15 60 4 \
"1" "[MasterDev]" \
"2" "[MasterSlave]"  3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo $OPTION | grep 1
    if [ $? -eq 0 ]; then
	    echo ""
		hostnamectl set-hostname dev-master
	    jenkins
	    docker
        orchestration
		monitering
	    curl ifconfig.io
	    echo "toilet -F border DevMaster  | lolcat" >> /etc/bash.bashrc
	    sudo cat /var/lib/jenkins/secrets/initialAdminPassword

	    

    else 
	    echo ""
		sudo apt install openjdk-8-jdk -y
		hostnamectl set-hostname slave-master
		docker
		orchestration
		jenkins_slave
		monitering
		echo "toilet -F border MasterSlave  | lolcat" >> /etc/bash.bashrc
	    curl ifconfig.io
	 

    fi 	    
else
    echo "You chose Cancel."
fi

else
    echo "You chose No. Exit status was $?."
fi #end of start or not


else #if of os version
whiptail --title "Test Message Box" --msgbox "Installation failed this is not an ubuntu" 10 60
fi #end of os checking



