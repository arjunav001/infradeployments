provider "aws" {
  region     = "us-east-1"
  access_key = "AKIAJPXQJ6RTGSKC5HPA"
  secret_key = "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"
}

resource "aws_security_group" "w2_security_group" {
  description = "W2 security group."

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 15672
    to_port   = 15672
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port   = 8082
    to_port     = 8082
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8083
    to_port     = 8083
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8084
    to_port     = 8084
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "ec2_instance" {
  ami             = "ami-04b9e92b5572fa0d1"
  instance_type   = "t2.micro"
  key_name        = "yoyo"
  security_groups = ["${aws_security_group.w2_security_group.name}"]
#  user_data 	  = "${data.template_file.user-data.rendered}"
#  user_data = <<-EOF
#                #!/bin/bash
#		sudo hostnamectl set-hostname prodMaster
#		sudo curl --tlsv1.2 --silent --show-error --header 'x-connect-key: 34910079b0c9176f7ec97e7c0013bd4ac155a013' https://kickstart.jumpcloud.com/Kickstart | sudo bash
#	
#		EOF

  tags = {
    Name  = "CommandCenter"
    Batch = "Production"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ec2_instance.public_ip} > public_ips.txt"
  }

  provisioner "file" {
    source      = "s.sh"
    destination = "/tmp/s.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod a+x /tmp/s.sh",
      "sleep 5",
      "sudo sh /tmp/s.sh",
      "sleep 10",
    ]
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    password    = ""
    host        = "${aws_instance.ec2_instance.public_ip}"
    private_key = "${file("./yoyo.pem")}"
  }
}

#data template_file "user-data"
#{
#    template = "${file("s.sh")}"
#}

output "showPublicIP" {
  value = "${aws_instance.ec2_instance.*.public_ip}"
}

