apt update -y
apt install apt-transport-https ca-certificates curl software-properties-common -y 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update -y
apt-cache policy docker-ce -y
apt install docker-ce -y
systemctl enable docker
systemctl restart docker
usermod -aG docker ${USER}
usermod -aG docker ubuntu
docker run hello-world
